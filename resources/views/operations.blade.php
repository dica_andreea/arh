@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{route('home')}}">Dashboard</a>
            </li>
            <li class="breadcrumb-item active">Lucrari</li>
        </ol>
        <!-- Example DataTables Card-->
        <div class="card mb-3">
            <div class="card-header">
                <i class="fas fa-building"></i> Lista Lucrari
                <a href="{{route('addOperation',['id'=>0])}}" class="btn btn-sm btn-primary float-right"><i class="fa fa-plus-circle"></i> Adauga lucrare</a>
                @if(Session::has('message'))
                    <p class="alert alert-success mt-3">{{ Session::get('message') }}</p>
                @endif
            </div>
            <div class="card-body">
                {{--<div class="row">--}}
                    {{--<div class="col-sm-3">--}}
                        {{--<select class="form-control searchSelect" name="id_client" id="select2_beneficiari"--}}
                                {{--data-url="{{route('search-customers')}}">--}}
                        {{--</select>--}}
                    {{--</div>--}}
                {{--</div>--}}
                <div class="table-responsive">
                    <table class="table table-bordered" id="operationsTable" width="100%" data-url="{{route('get-operations')}}"
                           cellspacing="0">
                        <thead>
                        <tr>
                            <th>Nr crt</th>
                            <th>Beneficiar</th>
                            <th>Angajat</th>
                            <th>Adaugat la</th>
                            <th>Actiuni</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection
@push('css')
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/css/tempusdominus-bootstrap-4.min.css"/>
@endpush
@push('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"
            integrity="sha256-4iQZ6BVL4qNKlQ27TExEhBN1HFPvAvAMbFavKKosSWQ=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/locale/ro.js"
            integrity="sha256-byt6+Ts3ia6sQPF6Yy/g+D11Lib6k3uXTfufCiFSGrY=" crossorigin="anonymous"></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/js/tempusdominus-bootstrap-4.min.js"></script>

    <script src="{{asset('js/operations.js')}}"></script>
@endpush