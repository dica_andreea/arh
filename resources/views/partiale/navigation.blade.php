<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <a class="navbar-brand" href="{{route('home')}}">Arhitecture</a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
            data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
            aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">

            @if (\Illuminate\Support\Facades\Auth::user()->admin === 1)
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Angajati">
                <a class="nav-link" href="{{route('users')}}">
                    <i class="fas fa-user-tag"></i>
                    <span class="nav-link-text">Angajati</span>
                </a>
            </li>
            @endif
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Clienti">
                <a class="nav-link" href="{{route('customers')}}">
                    <i class="fas fa-user"></i>
                    <span class="nav-link-text">Clienti</span>
                </a>
            </li>
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Avize/Studii">
                <a class="nav-link" href="{{route('approvals-studies')}}">
                    <i class="far fa-newspaper"></i>
                    <span class="nav-link-text">Avize/Studii</span>
                </a>
            </li>
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Lucrari">
                <a class="nav-link" href="{{route('operations')}}">
                    <i class="fas fa-building"></i>
                    <span class="nav-link-text">Lucrari</span>
                </a>
            </li>

        </ul>
        <ul class="navbar-nav sidenav-toggler">
            <li class="nav-item">
                <a class="nav-link text-center" id="sidenavToggler">
                    <i class="fa fa-fw fa-angle-left"></i>
                </a>
            </li>
        </ul>
        <ul class="navbar-nav ml-auto">
            <li class="nav-item text-info" style="line-height: 2.5;">
                Autentificat ca <a href="{{route('edit-user')}}"> <strong>{{\Illuminate\Support\Facades\Auth::user()->name}}</strong></a>
            </li>
            <li class="nav-item" style="line-height: 2.5;">
                <a class=nav-link" href="{{ route('logout') }}"  data-target="#exampleModal" data-toggle="modal"
                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    <i class="fa fa-fw fa-sign-out"></i>Logout
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </li>

        </ul>
    </div>
</nav>