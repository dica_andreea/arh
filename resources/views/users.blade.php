@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{route('home')}}">Dashboard</a>
            </li>
            <li class="breadcrumb-item active">Angajati</li>
        </ol>
        <!-- Example DataTables Card-->
        <div class="card mb-3">
            <div class="card-header">
                <i class="fas fa-user-tag"></i> Lista angajati
                    <a href="{{route('addUser',['id'=>0])}}" class="btn btn-sm btn-primary float-right"><i class="fa fa-plus-circle"></i> Adauga angajat</a>

            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="userTable" width="100%" data-url="{{route('get-users')}}"
                           cellspacing="0">
                        <thead>
                        <tr>
                            <th>Nume</th>
                            <th>Email</th>
                            <th>Adaugat la</th>
                            <th>Actualizat la</th>
                            <th>Actiuni</th>
                        </tr>
                        </thead>

                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection
@push('css')

@endpush
@push('js')
    <script src="{{asset('js/users.js')}}"></script>
@endpush