@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{route('home')}}">Dashboard</a>
            </li>
            <li class="breadcrumb-item active">Avize/Studii</li>
        </ol>
        <!-- Example DataTables Card-->
        <div class="card mb-3">
            <div class="card-header">
                <i class="far fa-newspaper"></i> Lista Avize/Studii
                <a href="{{route('addApproval',['id'=>0])}}" class="btn btn-sm btn-primary float-right"><i class="fa fa-plus-circle"></i> Adauga Aviz/Studiu</a>
                @if(Session::has('message'))
                    <div class="clearfix"></div>
                    <div class="alert alert-success mt-3">{{ Session::get('message') }}</div>
                @endif
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="approvalsTable" width="100%" data-url="{{route('get-approvals-studies')}}"
                           cellspacing="0">
                        <thead>
                        <tr>
                            <th>Nume</th>
                            <th>Status</th>
                            <th>Adaugat la</th>
                            <th>Actualizat la</th>
                            <th>Actiuni</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
        </div>
    </div>

@endsection
@push('css')

@endpush
@push('js')
    <script src="{{asset('js/approvals.js')}}"></script>
@endpush