@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">

            <li class="breadcrumb-item active">Dashboard</li>
        </ol>
        <div class="row">
            <div class="col-sm-4">
                <div class="card mb-3">
                    <div class="card-header">
                        <i class="fas fa-user-tag"></i> Angajati
                    </div>
                    <div class="card-body text-center">
                        {{$users}}
                    </div>

                </div>
            </div>
            <div class="col-sm-4">
                <div class="card mb-3">
                    <div class="card-header">
                        <i class="fas fa-user"></i>
                        Clienti
                    </div>
                    <div class="card-body text-center">
                        {{$customers}}
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="card mb-3">
                    <div class="card-header">
                        <i class="fas fa-building"></i>
                        Lucrari
                    </div>
                    <div class="card-body text-center">{{$operations}}</div>
                </div>
            </div>
        </div>
    </div>

@endsection
@push('css')

@endpush
@push('js')

@endpush