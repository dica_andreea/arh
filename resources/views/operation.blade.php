@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{route('home')}}">Dashboard</a>
            </li>
            <li class="breadcrumb-item active">Lucrari</li>
        </ol>

        <form action="{{action('HomeController@saveOperation')}}" method="post" id="addLucrare"
              class="{{isset($operation)?'edit':'add'}}">
            {{ csrf_field() }}
            <input hidden name="id" value="@isset($operation){{$operation->id}}@endisset">
            @error('id_client')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            @error('localitate')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            @error('telefon')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror

            <div class="row">
                @if(Session::has('message'))
                    <div class="col">
                        <div class="alert alert-success mt-3">{{ Session::get('message') }}</div>
                    </div>
                @endif
                <div class="col-sm-12 col-md-6">
                    <div class="card mb-3">
                        <div class="card-header">
                            <i class="fas fa-building"></i> Date indentificare beneficiar
                        </div>
                        <div class="card-body">
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label" for="select2_beneficiari">Nume S.C.:</label>
                                <div class="col-sm-8">
                                    <select class="form-control searchSelect" name="id_client" id="select2_beneficiari"
                                            data-url="{{route('search-customers')}}">
                                        @if($operation)
                                            <option value="{{$operation->customer->id}}">{{$operation->customer->nume}}</option>
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">

                                <label class="col-sm-4 col-form-label" for="localitate">Localitate:</label>
                                <div class="col-sm-8">
                                    <input class="form-control" name="localitate" type="text" placeholder="Localitate"
                                           value="@isset($operation){{$operation->customer->localitate}}@endisset">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label" for="telefon">Telefon:</label>
                                <div class="col-sm-8">
                                    <input class="form-control" name="telefon" type="text" placeholder="Telefon"
                                           value="@isset($operation){{$operation->customer->telefon}}@endisset">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label" for="lucrare">Lucrare:</label>
                                <div class="col-sm-8">
                                    <textarea class="form-control" name="lucrare" type="text"
                                              placeholder="Lucrare">@isset($operation){{$operation->lucrare}}@endisset</textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label" for="mentiuni">Alte mentiuni:</label>
                                <div class="col-sm-8">
                                    <textarea class="form-control" name="mentiuni" type="text"
                                              placeholder="Mentiuni">@isset($operation){{$operation->mentiuni}}@endisset</textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label" for="total_lucrare">Total lucrare (lei):</label>
                                <div class="col-sm-8">
                                    <input class="form-control" name="total_lucrare" type="text" placeholder="Total"
                                           value="@isset($operation){{$operation->total_lucrare}}@endisset">

                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label" for="avans_lucrare">Avans lucrare (lei):</label>
                                <div class="col-sm-8">
                                    <input class="form-control" name="avans_lucrare" type="text" placeholder="Avans"
                                           value="@isset($operation){{$operation->avans_lucrare}}@endisset">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6">
                    <div class="card mb-3">
                        <div class="card-header">
                            <i class="fa fa-table"></i> Repartizare lucrare
                        </div>
                        <div class="card-body">
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label" for="data">Data:</label>
                                <div class="col-sm-8">
                                    <div class="b4date input-group date" id="datetimepicker1"
                                         data-target-input="nearest">
                                        <input type="text" name="data" class="form-control datetimepicker-input"
                                               data-target="#datetimepicker1"
                                               @isset($operation) value="{{date('d.m.Y H:i', strtotime($operation->data))}} @endisset">
                                        <div class="input-group-append" data-target="#datetimepicker1"
                                             data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label" for="select2_angajat">D-l(a):</label>
                                <div class="col-sm-8">
                                    <select class="form-control searchSelect" name="id_angajat" id="select2_angajat"
                                            data-url="{{route('search-employees')}}">
                                        @if($operation)
                                            <option value="{{$operation->employee->id}}">{{$operation->employee->name}}</option>
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label" for="data_depunere_cu">Data depunerii
                                    C.U:</label>
                                <div class="col-sm-8">
                                    <div class="b4date input-group date" id="datetimepicker12"
                                         data-target-input="nearest">
                                        <input type="text" name="data_depunere_cu"
                                               class="form-control datetimepicker-input"
                                               data-target="#datetimepicker12"
                                               @isset($operation) value="{{date('d.m.Y H:i', strtotime($operation->data_depunere_cu))}}" @endisset/>
                                        <div class="input-group-append" data-target="#datetimepicker12"
                                             data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label" for="nr_cu_eliberat">Nr. C.U:</label>
                                <div class="col-sm-8">
                                    <input class="form-control" name="nr_cu_eliberat" type="text"
                                           placeholder="Nr. C.U"
                                           value="@isset($operation){{$operation->nr_cu_eliberat}}@endisset">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label" for="nume">Data eliberare C.U.:</label>
                                <div class="col-sm-8">
                                    <div class="b4date input-group date" id="datetimepicker13"
                                         data-target-input="nearest">
                                        <input type="text" name="data_cu_eliberat"
                                               class="form-control datetimepicker-input"
                                               data-target="#datetimepicker13"
                                               @isset($operation) value="{{date('d.m.Y H:i', strtotime($operation->data_cu_eliberat))}}" @endisset/>
                                        <div class="input-group-append" data-target="#datetimepicker13"
                                             data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input" name="beneficiar_cu"
                                            @isset($operation){{($operation->beneficiar_cu=='Y')?'checked':'' }}@endisset
                                            value="N">Predat
                                            beneficiar C.U.
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label" for="mentiuni">Alte mentiuni:</label>
                                <div class="col-sm-8">
                                    <textarea class="form-control" name="alte_mentiuni" type="text"
                                              placeholder="Mentiuni">@isset($operation){{$operation->alte_mentiuni}}@endisset</textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label" for="nume">Data primirii proiectelor de
                                    specialitate:</label>
                                <div class="col-sm-8">
                                    <div class="b4date input-group date" id="datetimepicker14"
                                         data-target-input="nearest">
                                        <input type="text" class="form-control datetimepicker-input"
                                               data-target="#datetimepicker14" name="data_primirii"
                                               @isset($operation) value="{{date('d.m.Y H:i', strtotime($operation->data_primirii))}}" @endisset>
                                        <div class="input-group-append" data-target="#datetimepicker14"
                                             data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label" for="rezistenta">Rezistenta:</label>
                                <div class="col-sm-8">
                                    <input class="form-control" name="rezistenta" type="text" placeholder="Rezistenta"
                                           value="@isset($operation){{$operation->rezistenta}}@endisset">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label" for="instalatii">Instalatii:</label>
                                <div class="col-sm-8">
                                    <input class="form-control" name="instalatii" type="text" placeholder="Instalatii"
                                           value="@isset($operation){{$operation->instalatii}}@endisset">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-6">
                    <div class="card mb-3">
                        <div class="card-header">
                            <i class="fa fa-table"></i> Avize necesare/studii
                        </div>
                        <div class="card-body">
                            @foreach($avize as $aviz)

                                <?php
                                $checked = '';
                                $data1 = $data2 = null;
                                if ($operation)
                                    foreach ($operation->avize as $a) {
                                        if ($a->id_aviz === $aviz->id) {
                                            $checked = 'checked';
                                            $data1 = date('d.m.Y H:i', strtotime($a->data_depunere));
                                            $data2 = date('d.m.Y H:i', strtotime($a->data_ridicare));
                                        }
                                    }
                                ?>

                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-check">
                                            <label class="form-check-label">
                                                <input type="checkbox" class="form-check-input"
                                                       name="avize[{{$aviz->id}}][id]"
                                                       {{$checked}} value="{{$aviz->id}}">{{$aviz->nume}}
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <div class="b4date input-group date" id="datetimepicker15{{$aviz->id}}"
                                                 data-target-input="nearest">
                                                <input type="text" class="form-control datetimepicker-input"
                                                       name="avize[{{$aviz->id}}][data_depunere]"
                                                       data-target="#datetimepicker15{{$aviz->id}}"
                                                       @isset($data1)value="{{$data1}}" @endisset
                                                       placeholder="Data depunere"/>
                                                <div class="input-group-append"
                                                     data-target="#datetimepicker15{{$aviz->id}}"
                                                     data-toggle="datetimepicker">
                                                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <div class="b4date input-group date" id="datetimepicker16{{$aviz->id}}"
                                                 data-target-input="nearest">
                                                <input type="text" class="form-control datetimepicker-input"
                                                       name="avize[{{$aviz->id}}][data_ridicare]"
                                                       data-target="#datetimepicker16{{$aviz->id}}"
                                                       @isset($data2)value="{{$data2}}" @endisset
                                                       placeholder="Data ridicare"/>
                                                <div class="input-group-append"
                                                     data-target="#datetimepicker16{{$aviz->id}}"
                                                     data-toggle="datetimepicker">
                                                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6">
                    <div class="card mb-3">
                        <div class="card-header">
                            <i class="fa fa-table"></i> Observatii DTAC/DTAD
                        </div>
                        <div class="card-body">

                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label" for="nume_obs">Nume:</label>
                                <div class="col-sm-8">
                                    <input class="form-control" name="nume_obs" type="text" placeholder="Numele"
                                           value="@isset($operation){{$operation->nume_obs}}@endisset">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label" for="data_finalizarii">Data finalizare:</label>
                                <div class="col-sm-8">
                                    <div class="b4date input-group date" id="datetimepicker17"
                                         data-target-input="nearest">
                                        <input type="text" class="form-control datetimepicker-input"
                                               data-target="#datetimepicker17" name="data_finalizarii"
                                               @isset($operation) value="{{date('d.m.Y H:i', strtotime($operation->data_finalizarii))}}" @endisset/>
                                        <div class="input-group-append" data-target="#datetimepicker17"
                                             data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label" for="data_predarii">Data predare:</label>
                                <div class="col-sm-8">
                                    <div class="b4date input-group date" id="datetimepicker18"
                                         data-target-input="nearest">
                                        <input type="text" class="form-control datetimepicker-input"
                                               data-target="#datetimepicker18" name="data_predarii"
                                               @isset($operation) value="{{date('d.m.Y H:i', strtotime($operation->data_predarii))}}" @endisset/>
                                        <div class="input-group-append" data-target="#datetimepicker18"
                                             data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label" for="alte_mentiuni_obs">Alte mentiuni:</label>
                                <div class="col-sm-8">
                                    <textarea class="form-control" name="alte_mentiuni_obs" type="text"
                                              placeholder="Mentiuni"
                                    >@isset($operation){{$operation->alte_mentiuni_obs}}@endisset</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col text-center mb-3">
                    <button type="submit" class="btn btn-primary btn-sm">Salveaza</button>

                </div>
            </div>
        </form>

    </div>

@endsection
@push('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/css/tempusdominus-bootstrap-4.min.css"/>
@endpush
@push('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"
            integrity="sha256-4iQZ6BVL4qNKlQ27TExEhBN1HFPvAvAMbFavKKosSWQ=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/locale/ro.js"
            integrity="sha256-byt6+Ts3ia6sQPF6Yy/g+D11Lib6k3uXTfufCiFSGrY=" crossorigin="anonymous"></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/js/tempusdominus-bootstrap-4.min.js"></script>

    <script src="{{asset('js/operations.js')}}"></script>
@endpush