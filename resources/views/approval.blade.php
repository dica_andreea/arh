@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{route('home')}}">Dashboard</a>
            </li>
            <li class="breadcrumb-item active">Avize/Studii</li>
        </ol>
        <!-- Example DataTables Card-->
        <div class="card mb-3">
            <div class="card-header">
                <i class="far fa-newspaper"></i> Aviz/Studiu
            </div>
            <div class="card-body">
                @error('nume')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <form action="{{action('HomeController@saveApproval')}}" method="post">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <input hidden name="id" value="@isset($approval){{$approval->id}}@endisset">
                        <div class="col-md-8">
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label" for="nume">Aviz/Studiu:</label>
                                <div class="col-sm-9">
                                    <input class="form-control" name="nume" type="text" placeholder="Aviz/Studiu"
                                           value="@isset($approval){{$approval->nume}}@endisset">
                                </div>
                            </div>
                        </div>
                        @if($approval)
                            <div class="col-md-8">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Status:</label>
                                    <div class="col-sm-9">
                                        <div class="form-check-inline">
                                            <label class="form-check-label">
                                                <input type="radio" class="form-check-input" name="status"
                                                       value="activ" {{($approval->status=='activ')?'checked':'' }}>activ
                                            </label>
                                        </div>
                                        <div class="form-check-inline">
                                            <label class="form-check-label">
                                                <input type="radio" class="form-check-input" name="status"
                                                       value="dezactivat"{{($approval->status=='dezactivat' || !$approval->status)?'checked':'' }}>dezactivat
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                    <button type="submit" class="btn btn-primary btn-sm">Salveaza</button>
                </form>
            </div>
        </div>
    </div>

@endsection
@push('css')

@endpush
@push('js')
    <script src="{{asset('js/approvals.js')}}"></script>
@endpush