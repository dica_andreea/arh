@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{route('home')}}">Dashboard</a>
            </li>
            <li class="breadcrumb-item active">Clienti</li>
        </ol>
        <!-- Example DataTables Card-->
        <div class="card mb-3">
            <div class="card-header">
                <i class="fas fa-user"></i> Lista Clienti
            </div>
            <div class="card-body">
                <form action="{{action('HomeController@saveCustomer')}}" method="post">
                    {{ csrf_field() }}
                    <div class="form-group">
                        @error('nume')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        @error('telefon')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        @error('localitate')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <input hidden name="id" value="@isset($customer){{$customer->id}}@endisset">
                        <div class="col-md-8">
                            <div class="form-group row">

                                <label class="col-sm-3 col-form-label" for="nume">Beneficiar:</label>
                                <div class="col-sm-9">
                                    <input class="form-control" name="nume" type="text" placeholder="Beneficiar"
                                           value="@isset($customer){{$customer->nume}}@endisset">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label" for="localitate">Localitate:</label>
                                <div class="col-sm-9">
                                    <input class="form-control" name="localitate" type="text" placeholder="Localitate"
                                           value="@isset($customer){{$customer->localitate}}@endisset">
                                </div>
                            </div>
                            <div class="form-group row">

                                <label class="col-sm-3 col-form-label" for="telefon">Telefon:</label>
                                <div class="col-sm-9">
                                    <input class="form-control" name="telefon" type="text" placeholder="Telefon"
                                           value="@isset($customer){{$customer->telefon}}@endisset">
                                </div>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary btn-sm">Salveaza</button>
                </form>
            </div>
        </div>
    </div>

@endsection
@push('css')

@endpush
@push('js')
    <script src="{{asset('js/customers.js')}}"></script>
@endpush