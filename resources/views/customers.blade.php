@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{route('home')}}">Dashboard</a>
            </li>
            <li class="breadcrumb-item active">Clienti</li>
        </ol>
        <!-- Example DataTables Card-->
        <div class="card mb-3">
            <div class="card-header">
                <i class="fas fa-user"></i> Lista Clienti
                <a href="{{route('addCustomer',['id'=>0])}}" class="btn btn-sm btn-primary float-right"><i class="fa fa-plus-circle"></i> Adauga client</a>
                @if(Session::has('message'))
                    <p class="alert alert-success mt-3">{{ Session::get('message') }}</p>
                @endif
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="customersTable" width="100%" data-url="{{route('get-customers')}}"
                           cellspacing="0">
                        <thead>
                        <tr>
                            <th>Nume</th>
                            <th>Localitate</th>
                            <th>Telefon</th>
                            <th>Adaugat la</th>
                            <th>Actualizat la</th>
                            <th>Actiuni</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection
@push('css')

@endpush
@push('js')
    <script src="{{asset('js/customers.js')}}"></script>
@endpush