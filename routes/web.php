<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::middleware(['auth','checkstatus'])->group(function () {

    Route::get('/users', 'HomeController@users')->name('users');
    Route::get('/user/edit', 'UserController@edit')->name('edit-user');
    Route::post('/user/save', 'UserController@save')->name('save-user');
    Route::get('/home', 'HomeController@index')->name('home');
    Route::post('/users/get', 'HomeController@getUsers')->name('get-users');

    Route::post('/userStatus/save/{id}', 'UserController@saveStatus')->name('save-user-status');

    Route::get('add-user/{id}', 'HomeController@addUser')->name('addUser');

    //clienti
    Route::get('/customers', 'HomeController@customers')->name('customers');
    Route::post('/customers/get', 'HomeController@getCustomers')->name('get-customers');
    Route::get('/customers/search', 'HomeController@searchCustomers')->name('search-customers');
    Route::get('/emplyees/search', 'HomeController@searchEmployees')->name('search-employees');
    Route::get('add-customer/{id}', 'HomeController@addCustomer')->name('addCustomer');
    Route::post('addCustomer', 'HomeController@saveCustomer')->name('saveCustomer');

    //avize/studii
    Route::get('/approvals-studies', 'HomeController@approvals')->name('approvals-studies');
    Route::post('/approvals-studies/get', 'HomeController@getApprovals')->name('get-approvals-studies');
    Route::get('add-approval/{id}', 'HomeController@addApproval')->name('addApproval');
    Route::post('addApproval', 'HomeController@saveApproval')->name('saveApproval');

    //lucrari
    Route::get('/operations', 'HomeController@operations')->name('operations');
    Route::post('/operations/get', 'HomeController@getOperations')->name('get-operations');
    Route::get('add-operation/{id}', 'HomeController@addOperation')->name('addOperation');
    Route::post('addOperation', 'HomeController@saveOperation')->name('saveOsperation');
});

Auth::routes();
