<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAvizeLucrariTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('avize_lucrari', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_aviz')->unsigned();
            $table->integer('id_lucrare')->unsigned();
            $table->foreign('id_aviz')->references('id')->on('avize_studii');
            $table->foreign('id_lucrare')->references('id')->on('avize_lucrari');
            $table->timestamp('data_depunere')->nullable();
            $table->timestamp('data_ridicare')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('avize_lucrari');
    }
}
