<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLucrariTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lucrari', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_angajat');
            $table->integer('id_client');
            $table->string('lucrare')->nullable();
            $table->string('mentiuni')->nullable();
            $table->float('total_lucrare')->nullable();
            $table->float('avans_lucrare')->nullable();
            $table->timestamp('data')->nullable();
            $table->timestamp('data_depunere_cu')->nullable();
            $table->integer('nr_cu_eliberat')->nullable();
            $table->timestamp('data_cu_eliberat')->nullable();
            $table->enum('beneficiar_cu', ['Y', 'N'])->default('N');
            $table->string('alte_mentiuni')->nullable();
            $table->timestamp('data_primirii')->nullable();
            $table->string('rezistenta')->nullable();
            $table->string('instalatii')->nullable();
            $table->string('nume_obs')->nullable();
            $table->timestamp('data_finalizarii')->nullable();
            $table->timestamp('data_predarii')->nullable();
            $table->string('alte_mentiuni_obs')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lucrari');
    }
}
