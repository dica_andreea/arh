$(document).ready(function () {
    var url = $('#customersTable').data('url');
    $('#customersTable').DataTable({
        pageLength: 10,
        processing: true,
        serverSide: true,
        searchDelay: 400,
        deferRender: true,
        ajax: {
            type: 'POST',
            url: url,
        },
        pagingType: 'simple_numbers',
        columns: [
            {
                data: 'nume',
                name: 'nume',
            },
            {
                data: 'localitate',
            },
            {
                data: 'telefon',
                orderable: false,
            },
            {
                data: 'created_at',
                orderable: false,
            },
            {
                data: 'updated_at',
            },

            {
                data: null,
                orderable: false,
                className: 'text-center',
                render: function (a, b, object) {

                    return generateButtons(object);
                }
            },

        ],
        language: {
            loadingRecords: 'Se incarca rezultate',
            processing: 'Se incarca ...',
        }
    });

    var dtable = $('#customersTable').dataTable().api();

    /**
     * doar dupa ce au fost tastate 3 caractere incepem cautare
     */
    $('.dataTables_filter input').unbind().bind('input', function (e) { //  Unbind previous default bindings / Bind our desired behavior

        if (this.value.length >= 3 || e.keyCode == 13)// If the length is 3 or more characters, or the user pressed ENTER, search
            dtable.search(this.value).draw(); // Call the API search function

        if (this.value == '') // Ensure we clear the search if they backspace far enough
            dtable.search('').draw();

        return;
    });
});
function generateButtons(object) {
    return  '<a class="btn btn-outline-success btn-sm mr-1" href="./add-customer/' + object.id + '"> <i class="fas fa-pen"></i></a>';

}