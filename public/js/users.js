$(document).ready(function () {

    $('body').on('click', '.changeStatus', function (e) {
        console.log($(this).is(":checked"));
        var id = $(this).val();
        console.log(id);
        var status = 'activ';
        if (!$(this).is(":checked"))
            status = 'inactiv';
        //
        $.ajax({
            method: 'post',
            data: {id: id, status: status},
            url: './userStatus/save/' + id,
            success: function (data) {

            },
        });
    });

    var url = $('#userTable').data('url');
    $('#userTable').DataTable({
        pageLength: 10,
        processing: true,
        serverSide: true,
        searchDelay: 400,
        deferRender: true,
        ajax: {
            type: 'POST',
            url: url,
        },
        pagingType: 'simple_numbers',
        columns: [
            {
                data: 'name',
                name: 'nume',
            },
            {
                data: 'email',
            },
            {
                data: 'created_at',
            },
            {
                data: 'updated_at',
            },

            {
                data: null,
                className: 'text-center',
                render: function (a, b, object) {

                    return generateButtons(object);
                }
            },

        ],
        language: {
            loadingRecords: 'Se incarca rezultate',
            processing: 'Se incarca ...',
        }
    });

    var dtable = $('#userTable').dataTable().api();

    /**
     * doar dupa ce au fost tastate 3 caractere incepem cautare
     */
    $('.dataTables_filter input').unbind().bind('input', function (e) { //  Unbind previous default bindings / Bind our desired behavior

        if (this.value.length >= 3 || e.keyCode == 13)// If the length is 3 or more characters, or the user pressed ENTER, search
            dtable.search(this.value).draw(); // Call the API search function

        if (this.value == '') // Ensure we clear the search if they backspace far enough
            dtable.search('').draw();

        return;
    });
});

function generateButtons(object) {
    var checked = '';
    var disabled = '';
    if (object.status === 'activ')
        checked = 'checked';

    if (object.admin === 1)
       return  'ADMIN';

    btn = '    <label class="switch">  <input ' + disabled + ' ' + checked + ' class="changeStatus"  type="checkbox" value="' + object.id + '"> <span class="slider round"></span>' +
        '   </label>';


    return btn;
}