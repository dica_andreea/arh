$(document).ready(function () {

    $('.b4date').datetimepicker();
    $(".searchSelect").select2({
        ajax: {
            url: function () {
                return this[0].dataset.url
            },
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    q: params.term, // search term
                    page: params.page
                };
            },
            processResults: function (data, params) {
                // parse the results into the format expected by Select2
                // since we are using custom formatting functions we do not need to
                // alter the remote JSON data, except to indicate that infinite
                // scrolling can be used
                params.page = params.page || 1;
                return {
                    results: data.items,
                    pagination: {
                        more: (params.page * 10) < data.total_count
                    }
                };
            },
            cache: true
        },
        placeholder: 'Cauta...',
        escapeMarkup: function (markup) {
            return markup;
        }, // let our custom formatter work
        minimumInputLength: 2,
        templateResult: formatRepo,
        templateSelection: formatRepoSelection
    });

    var url = $('#operationsTable').data('url');
    $('#operationsTable').DataTable({
        pageLength: 10,
        processing: true,
        serverSide: true,
        searchDelay: 400,
        deferRender: true,
        ajax: {
            type: 'POST',
            url: url,
        },
        pagingType: 'simple_numbers',
        columns: [
            {
                data: 'id',
                name:'Nr crt',
            },
            {
                data: 'customer.nume',
                name:'beneficiar',
            },
            {
                data: 'employee.name',
                name:'angajat',
            },
            {
                data: 'created_at',
            },
            {
                data: null,
                className: 'text-center',
                orderable: false,
                render: function (a, b, object) {

                    return generateButtons(object);
                }
            },

        ],
        language: {
            loadingRecords: 'Se incarca rezultate',
            processing: 'Se incarca ...',
        }
    });

    var dtable = $('#operationsTable').dataTable().api();

    /**
     * doar dupa ce au fost tastate 3 caractere incepem cautare
     */
    $('.dataTables_filter input').unbind().bind('input', function (e) { //  Unbind previous default bindings / Bind our desired behavior

        if (this.value.length >= 3 || e.keyCode == 13)// If the length is 3 or more characters, or the user pressed ENTER, search
            dtable.search(this.value).draw(); // Call the API search function

        if (this.value == '') // Ensure we clear the search if they backspace far enough
            dtable.search('').draw();

        return;
    });
});
function generateButtons(object) {
    var edit = '<a class="btn btn-outline-success btn-sm mr-1" href="./add-operation/' + object.id + '"> <i class="fas fa-pen"></i></a>';
    var buttons =  edit;
    return buttons;
}
function formatRepo(repo) {
    return "<div class='select2-result-repository__statistics'>" + repo.text + "</div>";
}
function formatRepoSelection(repo) {
    return repo.text || repo.id;
}