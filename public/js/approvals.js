$(document).ready(function () {
    var url = $('#approvalsTable').data('url');
    $('#approvalsTable').DataTable({
        pageLength: 10,
        processing: true,
        serverSide: true,
        searchDelay: 400,
        deferRender: true,
        ajax: {
            type: 'POST',
            url: url,
        },
        pagingType: 'simple_numbers',
        columns: [
            {
                data: 'nume',
                name: 'nume',
            },
            {
                data: 'status',
                className: 'text-center',
                render: function (a) {
                    if (a == 'activ') {
                        return '<i class="far fa-check-circle" style="color: #008000;"></i>';
                    } else {
                        return '<i class="fas fa-minus-circle" style="color: #ff0000"></i>';
                    }


                }
            }, {
                data: 'created_at',
            },
            {
                data: 'updated_at',
            },

            {
                data: null,
                orderable: false,
                className: 'text-center',
                render: function (a, b, object) {

                    return generateButtons(object);
                }
            },

        ],
        language: {
            loadingRecords: 'Se incarca rezultate',
            processing: 'Se incarca ...',
        }
    });

    var dtable = $('#approvalsTable').dataTable().api();

    /**
     * doar dupa ce au fost tastate 3 caractere incepem cautare
     */
    $('.dataTables_filter input').unbind().bind('input', function (e) { //  Unbind previous default bindings / Bind our desired behavior

        if (this.value.length >= 3 || e.keyCode == 13)// If the length is 3 or more characters, or the user pressed ENTER, search
            dtable.search(this.value).draw(); // Call the API search function

        if (this.value == '') // Ensure we clear the search if they backspace far enough
            dtable.search('').draw();

        return;
    });
});

function generateButtons(object) {
    var edit = '<a class="btn btn-outline-success btn-sm mr-1" href="./add-approval/' + object.id + '"> <i class="fas fa-pen"></i></a>';
    buttons = edit;
    return buttons;
}