<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;
class Operation extends Model
{
    public $table = 'lucrari';
    public function customer()
    {
        return $this->hasOne('App\Models\Customer', 'id', 'id_client');
    }
    public function employee()
    {
        return $this->hasOne('App\User', 'id', 'id_angajat');
    }

    public function avize_lucrari()
    {
        return $this->belongsToMany('App\Models\Approval');
    }
}