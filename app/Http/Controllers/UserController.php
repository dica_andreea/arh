<?php

namespace App\Http\Controllers;

use App\Http\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public $userService;
    public $approvalsService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->userService = new UserService();
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function edit()
    {
        $this->data['user'] = Auth::user();

        return view('user.edit', $this->data);
    }

    public function save(Request $request)
    {

        $this->userService->save($request->all());

        return redirect()->back();
    }

    public function saveStatus($id)
    {
        $this->userService->saveStatus($id);
    }


}
