<?php

namespace App\Http\Controllers;

use App\Http\Requests\ApprovalRequest;
use App\Http\Requests\CustomerRequest;
use App\Http\Requests\OperationRequest;
use App\Http\Services\ApprovalsService;
use App\Http\Services\CustomerService;
use App\Http\Services\OperationService;
use App\Http\Services\UserService;
use App\Models\Approval;
use App\Models\AvizeLucrari;
use App\Models\Customer;
use App\Models\Operation;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public $userService;
    public $approvalsService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->userService = new UserService();
        $this->customerService = new CustomerService();
        $this->approvalsService = new ApprovalsService();
        $this->operationService = new OperationService();
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $users=User::count();
        $customers=Customer::count();
        $operations=Operation::count();

        return view('home', ['users'=>$users, 'customers'=>$customers, 'operations'=>$operations]);
    }

    public function users()
    {
        if (!Auth::user()->admin)
            abort(401);

        return view('users', $this->data);
    }

    public function getUsers(Request $request)
    {
        if (!Auth::user()->admin)
            return [];

        return $this->userService->getUsers($request->all());
    }

    public function addUser($id)
    {
        $user = User::find($id);
        return view('user.add', ['user' => $user]);
    }

    public function customers()
    {
        return view('customers', $this->data);
    }

    public function getCustomers(Request $request)
    {
        return $this->customerService->getCustomers($request->all());
    }

    public function searchEmployees(Request $request)
    {
        $data = $this->customerService->searchEmployees($request->get('q'), $request->get('page'));

        return response()->json($data);
    }

    public function searchCustomers(Request $request)
    {
        $data = $this->customerService->searchCustomers($request->get('q'), $request->get('page'));

        return response()->json($data);
    }

    public function addCustomer($id)
    {
        $customer = Customer::find($id);

        return view('customer', ['customer' => $customer]);
    }

    public function saveCustomer(CustomerRequest $request)
    {
        $this->customerService->save($request->all());

        return redirect('customers');
    }

    public function approvals()
    {
        return view('approvals', $this->data);
    }

    public function getApprovals(Request $request)
    {
        return $this->approvalsService->getApprovals($request->all());
    }

    public function addApproval($id)
    {
        $approval = Approval::find($id);

        return view('approval', ['approval' => $approval]);
    }

    public function saveApproval(ApprovalRequest $request)
    {

        $this->approvalsService->save($request->all());

        return redirect('approvals-studies');
    }

    public function operations()
    {
        return view('operations', $this->data);
    }

    public function getOperations(Request $request)
    {
        return $this->operationService->getOperations($request->all());
    }

    public function addOperation($id)
    {
        $operation = $this->operationService->getFullOperation($id);
        $avize = Approval::all();
        return view('operation', ['operation' => $operation, 'avize' => $avize]);
    }

    public function saveOperation(OperationRequest $request)
    {
        $postData = $request->all();

        $this->operationService->save($postData);


        return redirect()->route('operations');
    }

}
