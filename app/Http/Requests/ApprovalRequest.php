<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ApprovalRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'nume' => 'required|max:200|min:3|string',
        ];
    }

    public function messages() {
        return [
            'nume.required' => 'Numele este obligatoriu!',
            'nume.max' => 'Maximul de caractere este 200',
            'nume.min' => 'Minumul de caractere este 3',
        ];
    }

//    public function response(array $errors) {
//
//        $data = array(
//            "status" => "invalid_data",
//            "message" => $errors,
//        );
//
//        return response()->json($data, 422);
//    }

}
