<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OperationRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'id_angajat' => 'required',
            'id_client' => 'required',
            'lucrare' => 'required|string',
            'mentiuni' => 'string',
            'total_lucrare' => 'numeric',
            'avans_lucrare' => 'numeric',
            'data' => 'date',
            'data_depunere_cu' => 'date',
            'nr_cu_eliberat' => 'numeric',
            'data_cu_eliberat' => 'date',
            'alte_mentiuni' => 'nullable|string',
            'data_primirii' => 'date',
            'rezistenta' => 'nullable|string',
            'instalatii' => 'nullable|string',
            'nume_obs' => 'nullable|string',
            'data_finalizarii' => 'date',
            'data_predarii' => 'date',
            'alte_mentiuni_obs' => 'nullable|string',
        ];
    }

}
