<?php
/**
 * Created by PhpStorm.
 * User: Andreea
 * Date: 11-Sep-19
 * Time: 10:44 PM
 */

namespace App\Http\Services;

use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class UserService
{

    public function getUsers($data)
    {

        $search = $data['search']['value'];
        $length = $data['length'];
        $start = $data['start'];
        $orderCol = $data['order'][0]['column'];
        $orderDir = $data['order'][0]['dir'];
        $orderCols = [
            0 => 'name',
        ];

        $users = User::whereRaw('1=1');
        $recordsTotal = $users->count();
        if ($search)
            $users->where('name', 'LIKE', '%' . $search . '%');

        if (isset($orderCols[$orderCol]))
            $users->orderBy($orderCols[$orderCol], $orderDir);

        $recordsFiltered = $users->count();
        $users = $users->take($length)->skip($start)->get();


        return response()->json(['data' => $users, 'recordsTotal' => $recordsTotal, 'recordsFiltered' => $recordsFiltered]);
    }

    public function save($postData)
    {

        $user = User::find(Auth::user()->id);

        $user->name = $postData['name'];

        if (isset($postData['old_password'])) {
            if (!Auth::attempt(['email' => Auth::user()->email, 'password' => $postData['old_password'],
                'password_confirmation' => $postData['old_password']])) {
                Session::flash('message', 'Vechea parola nu corespunde!');
                return false;
            }
            if (isset($postData['password']) && isset($postData['password_confirmation'])) {
                if ($postData['password'] !== $postData['password_confirmation']) {
                    Session::flash('message', 'Confirma noua parola');
                    return false;
                }
                $user->password = Hash::make($postData['password']);
            }
        }

        $user->save();

        Session::flash('message', 'Contul a fost salvat cu succes!');
        return true;
    }

    public function saveStatus($id)
    {
        $user = User::find($id);
        if ($user->status == 'activ') {
            $user->status = 'inactiv';
        } else {
            $user->status = 'activ';
        }
        $user->save();
    }

}