<?php

namespace App\Http\Services;

use App\Models\Approval;
use Illuminate\Support\Facades\Session;

class ApprovalsService
{

    public function getApprovals($data)
    {
        $search = $data['search']['value'];
        $length = $data['length'];
        $start = $data['start'];
        $orderCol = $data['order'][0]['column'];
        $orderDir = $data['order'][0]['dir'];
        $orderCols = [
            0 => 'nume',
            1 => 'status',
            2 => 'created_at',
            3 => 'updated_at',
        ];

        $approvals = Approval::whereRaw('1=1');
        $recordsTotal = $approvals->count();
        if ($search)
            $approvals->where('nume', 'LIKE', '%' . $search . '%');

        if (isset($orderCols[$orderCol]))
            $approvals->orderBy($orderCols[$orderCol], $orderDir);

        $recordsFiltered = $approvals->count();
        $approvals = $approvals->take($length)->skip($start)->get();

        return response()->json(['data' => $approvals, 'recordsTotal' => $recordsTotal, 'recordsFiltered' => $recordsFiltered]);
    }

    public function save($postData)
    {
        $approval = Approval::find($postData['id']);

        if (!$approval) {
            $approval = new Approval();
        }
        $approval->nume = $postData['nume'];
        $approval->status = isset($postData['status']) ? $postData['status'] : 'activ';

        $approval->save();
        Session::flash('message', 'Aviz/Studiu "' . $approval->nume . '" a fost salvat cu succes!');
    }

}