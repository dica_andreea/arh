<?php

namespace App\Http\Services;

use App\Models\ApprovalOperation;
use App\Models\Operation;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class OperationService
{

    public function getOperations($data)
    {

        $search = $data['search']['value'];
        $length = $data['length'];
        $start = $data['start'];
//        $beneficiar = isset($data['beneficiar']) ? $data['beneficiar'] : null;
        $orderCol = $data['order'][0]['column'];
        $orderDir = $data['order'][0]['dir'];
        $orderCols = [
            0 => 'id',
            1 => 'id_angajat',
            2 => 'id_client',
            3 => 'created_at',
        ];

        $operations = Operation::with(['customer', 'employee']);

        $recordsTotal = $operations->count();
        if ($search)
            $operations->where('lucrare', 'LIKE', '%' . $search . '%');
//        if ($beneficiar)
//            $operations->whereIdCustomer($beneficiar);
        if (isset($orderCols[$orderCol]))
            $operations->orderBy($orderCols[$orderCol], $orderDir);

        $recordsFiltered = $operations->count();
        $operations = $operations->take($length)->skip($start)->get();


        return response()->json(['data' => $operations, 'recordsTotal' => $recordsTotal, 'recordsFiltered' => $recordsFiltered]);
    }

    public function getFullOperation($id)
    {
        $operation = Operation::with(['customer', 'employee'])->find($id);
        if ($operation)
            $operation->avize = DB::table('avize_studii')
                ->join('avize_lucrari', 'avize_studii.id', '=', 'avize_lucrari.id_aviz')
                ->where('avize_lucrari.id_lucrare', $id)
                ->get();
        return $operation;
    }

    public function save($postData)
    {

        $operation = Operation::find($postData['id']);
        if (!$operation)
            $operation = new Operation();

        $operation->id_angajat = $postData['id_angajat'];
        $operation->id_client = $postData['id_client'];
        $operation->lucrare = $postData['lucrare'];
        $operation->mentiuni = $postData['mentiuni'];
        $operation->total_lucrare = $postData['total_lucrare'];
        $operation->avans_lucrare = $postData['avans_lucrare'];
        $operation->data = date('Y-m-d H:i', strtotime($postData['data']));
        $operation->data_depunere_cu = date('Y-m-d H:i', strtotime($postData['data_depunere_cu']));
        $operation->data_cu_eliberat = date('Y-m-d H:i', strtotime($postData['data_cu_eliberat']));
        $operation->data_primirii = date('Y-m-d H:i', strtotime($postData['data_primirii']));
        $operation->data_finalizarii = date('Y-m-d H:i', strtotime($postData['data_finalizarii']));
        $operation->data_predarii = date('Y-m-d H:i', strtotime($postData['data_predarii']));
        $operation->nr_cu_eliberat = $postData['nr_cu_eliberat'];
        $operation->beneficiar_cu = isset($postData['beneficiar_cu']) ? 'Y' : 'N';
        $operation->alte_mentiuni = $postData['alte_mentiuni'];
        $operation->rezistenta = $postData['rezistenta'];
        $operation->instalatii = $postData['instalatii'];
        $operation->nume_obs = $postData['nume_obs'];
        $operation->alte_mentiuni_obs = $postData['alte_mentiuni_obs'];
        $operation->save();

        ApprovalOperation::whereIdLucrare($operation->id)->delete(); // le stergem pe cele vechi daca exista

        foreach ($postData['avize'] as $key => $value) {


            if (isset($value['id']) && $value['id']) {
                $aviz = new ApprovalOperation();
                $aviz->id_aviz = $value['id'];
                $aviz->id_lucrare = $operation->id;
                $aviz->data_depunere = date('Y-m-d H:i', strtotime($value['data_depunere']));
                $aviz->data_ridicare = date('Y-m-d H:i', strtotime($value['data_ridicare']));
                $aviz->save();
//                dd($aviz);
            }
        }
        Session::flash('message', 'Lucrarea a fost salvata cu succes!');

        return $operation->id;
    }
}