<?php
/**
 * Created by PhpStorm.
 * User: Andreea
 * Date: 11-Sep-19
 * Time: 10:44 PM
 */

namespace App\Http\Services;

use App\Models\Customer;
use App\User;
use Illuminate\Support\Facades\Session;

class CustomerService
{

    public function getCustomers($data)
    {

        $search = $data['search']['value'];
        $length = $data['length'];
        $start = $data['start'];
        $orderCol = $data['order'][0]['column'];
        $orderDir = $data['order'][0]['dir'];
        $orderCols = [
            0 => 'nume',
            1 => 'localitate',
            2 => 'telefon',
            3 => 'created_at',
            4 => 'updated_at',
        ];

        $customers = Customer::whereRaw('1=1');
        $recordsTotal = $customers->count();
        if ($search)
            $customers->where('nume', 'LIKE', '%' . $search . '%');

        if (isset($orderCols[$orderCol]))
            $customers->orderBy($orderCols[$orderCol], $orderDir);

        $recordsFiltered = $customers->count();
        $customers = $customers->take($length)->skip($start)->get();


        return response()->json(['data' => $customers, 'recordsTotal' => $recordsTotal, 'recordsFiltered' => $recordsFiltered]);
    }

    public function searchEmployees($q, $start)
    {

        $users = User::selectRaw('id, name as text')
            ->where('name', 'LIKE', '%' . $q . '%')
            ->take(10)->skip($start)->get();

        return ['items' => $users, 'more' => true];
    }


    public function searchCustomers($q, $start)
    {

        $customers = Customer::selectRaw('id, nume as text')
            ->where('nume', 'LIKE', '%' . $q . '%')->take(10)->skip($start)->get();

        return ['items' => $customers, 'more' => true];
    }

    public function save($postData)
    {
        $customer = Customer::find($postData['id']);

        if (!$customer) {
            $customer = new Customer();
        }
        $customer->nume = $postData['nume'];
        $customer->localitate = $postData['localitate'];
        $customer->telefon = $postData['telefon'];

        $customer->save();
        Session::flash('message', 'Clientul "' . $customer->nume . '" a fost salvat cu succes!');

    }
}